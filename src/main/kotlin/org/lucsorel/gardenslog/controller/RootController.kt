package org.lucsorel.gardenslog.controller

import io.micrometer.core.annotation.Timed
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestParam

import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.util.concurrent.atomic.AtomicLong

data class Greeting(val id: Long, val content: String)

//@RestController("/")
@RestController
class RootController {

    val counter = AtomicLong()

    @Timed
    @GetMapping("/greeting")
    fun greeting(@RequestParam(value = "name", defaultValue = "World") name: String) =
            Mono.just(Greeting(counter.incrementAndGet(), "Hello, $name"))

}

