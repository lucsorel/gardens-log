package org.lucsorel.gardenslog.controller

import org.springframework.ui.ModelMap
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RestController
import reactor.core.publisher.Mono

@RestController
class UserController {

    @GetMapping("/user")
    fun getUser(model: MutableMap<String, Any>) : String {
        model.put("firstname", "Luc")
        model.put("lastname", "Sorel-Giffo")
        return "user"
//        return Mono.just("user")
    }
}