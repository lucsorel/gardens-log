package org.lucsorel.gardenslog

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class GardenslogApplication

fun main(args: Array<String>) {
    runApplication<GardenslogApplication>(*args)
}
