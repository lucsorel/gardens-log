
# Thymeleaf & webflux

* https://docs.spring.io/spring/docs/5.0.7.RELEASE/spring-framework-reference/web-reactive.html#webflux-view-thymeleaf
* https://github.com/thymeleaf/thymeleafsandbox-stsm-reactive
* http://javaetmoi.com/2017/12/migration-spring-web-mvc-vers-spring-webflux/#more-1794

# Database initialization

* [psql documentation](https://www.postgresql.org/docs/9.6/static/app-psql.html)

```sh
# lists schemas
\dn
# lists tables, views and sequences
\dp
# lists users
\du
# lists databases
\l
```

* launch the docker-compose stack
* log in the postgreSQL container

```sh
docker exec -it stack_db_1 /bin/bash
```

Failing to create db & user: https://github.com/docker-library/postgres/issues/41
